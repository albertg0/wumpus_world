#ifndef MAP_H
#define MAP_H

#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include <cstring>
#include <string>
#include <cstdlib>
#include <algorithm>
#include "node.h"
#include <iostream>
#include <sstream>
#include "Character.h"

/*
    Map.h
    
    The map object handles constructing the map.
    IT holds the double pointer nodes adjaceny matrix that handles
    the logic behind the map shown on the screen.
    It contains the agent,the size of the map, the orientation the player
    faces and the position of the gold.
    

*/

using namespace std;
class Map{

	
	public:
    Character agent;
	Node **nodes;
	int n;
	int x,y, orientation;
    int goldX, goldY;
    int startLocX,startLocY;
	string orient[8]; 
   
    Map();
    //Map(string);
    
    
    void setAgentOrientation(ifstream& );
    void setStartLocation(ifstream& ,int &,int &);
    void printConnections();
    void setOrientation(string a);
    int getOrientation();
    int getGoldX();
    int getGoldY();

    //Removes spaces ffrom any input using algorithm library
    string removeSpaces(string input);
    //Set player on the map
    void setPlayerString(int ,int ,int );
    void setPlayerString(int);
    void printSearch(int, int, string);
    void setCost(int, int, int);
    //Set the pits
    void setPits(ifstream &);
    //Set the gold
    void setGold(ifstream &);
    //Set the wumpus
    void setWumpus(ifstream & );
    void setBreeze();
    void setBreezePrecepts(Node* n);
                    
    void setStench();
    //prints grid
    void printGrid();
    
    //connects all the nodes
    void connectNodes();	
};

/*
    Map Constructor
    Default
    

*/

Map::Map()
{	 
    orient[0] = "^ ";
    orient[1] = "^>";
    orient[2] = "> ";
    orient[3] = "v>";
    orient[4] = "v ";
    orient[5] = "<v";
    orient[6] = "< ";
    orient[7] = "<^";
    
    

    startLocX = startLocY = 0;
    
    string filename = "wumpus_2.txt";
    ifstream configFile(filename.c_str());
    
    if(!configFile.is_open())
    {   
        cout << "using default";
        configFile.open("wumpus_1.txt");
    }
        
    
    string gridSize="";
    if(configFile)
    {  
        int posX =0,posY = 0;
  
        getline(configFile,gridSize);
        n = atoi(gridSize.c_str());

        nodes = new Node*[n];
        for(int i=0;i<n;i++)
            nodes[i] = new Node[n];
            
     
        for(int i=0;i<n;i++)
            for(int j=0;j<n;j++)
            {
                nodes[i][j].posX = i;
                nodes[i][j].posY = j ;
               
            }
        //connect all the nodes
        connectNodes();
        setStartLocation(configFile,posX,posY);
        agent =  Character(posX,posY);
        
        // Player Orientation
        setAgentOrientation(configFile);
    
        // set player on map
        setPlayerString(posX,posY, orientation);
                
        //Pits
        setPits(configFile);

        //Set gold				
        setGold(configFile);
        
        //Set Wumpus
        setWumpus(configFile);
        //Setting Breeze & Stench
        //setBreeze();
        setStench();
                
    }
    else
    { cout << "Couldnt open file."; }
    
}
void Map::setOrientation(string a)
{
			
			//turn player orientation left
			if(a == "L")
			{
				if(orientation <= 0)
				{
					orientation = 8;
				}
			
				orientation -= 1;
			}
			//turn player orientation right
			else
			{
				if(orientation == 7)
				{
					orientation = -1;			
				}
				
				orientation += 1;
			}
}
int Map::getOrientation(){
			return orientation;
}

int Map::getGoldX()
{ return goldX; }
int Map::getGoldY()
{ return goldY; }
				
void Map::setAgentOrientation(ifstream& file)
{
	//setting orientation properties
    string o = "";
    getline(file,o);
    if(atoi(o.c_str()) <= 0)
        orientation = 0;
    else
       {
            orientation = atoi(o.c_str());
            orientation--;
       }    
}


void Map::setStartLocation(ifstream& file,int &posX,int &posY)
{
        //Player start location
    string startLocation = "";
    getline(file,startLocation);
    string strX = startLocation.substr(0,2);
    string strY = startLocation.substr(2,3);
        
    posX = atoi(strX.c_str());
    posY = atoi(strY.c_str());
    startLocX = posX;
    startLocY = posY;
        
}

string Map::removeSpaces(string input)
{
    input.erase(remove(input.begin(),input.end(),' '),input.end());
    return input;
}

void Map::setPlayerString(int posX,int posY,int o)
{
    agent.currentNode = &nodes[posY][posX];
    nodes[posY][posX].hasPlayer = true;
    nodes[posY][posX].s[6] = "|      " + orient[o].substr(0,2) + "     |" ;
}

void Map::setPlayerString(int o)
{
    nodes[agent.posY][agent.posX].s[6] = "|      " + orient[o].substr(0,2) + "     |" ;
}

void Map::setPits(ifstream &file)
{
    string pitLine = "";
    getline(file,pitLine);
    string pits = removeSpaces(pitLine);
        
    int pX=0,pY=0;
    string strX="";
    string strY="";
    for(int i = 0; i<pits.length()/2; i++)
    {		
        strX = pits[i*2];
        strY = pits[i*2+1];
            
        pY = atoi(strX.c_str());
        pX = atoi(strY.c_str());
        
        nodes[pX][pY].hasPit = true;
        nodes[pX][pY].s[2] =  "|     Pit     |" ;
        //pass node[x][y] then in the function
        //check all nodes around it for setting breeze?
        setBreezePrecepts(&nodes[pX][pY]);
    }

}

void Map::setBreezePrecepts(Node* n)
{
   if(n->north)
    {
        if(!n->north->hasPit)
        {
            n->north->breeze = true;
            n->north->s[5] = "|    Breeze   |" ;
        }
    }
    if(n->south)
    {
        if(!n->south->hasPit)
        {
            n->south->breeze = true;
            n->south->s[5] = "|    Breeze   |" ;
        }
    }
    if(n->west)
    {
        if(!n->west->hasPit)
        {
            n->west->breeze = true;
            n->west->s[5] = "|    Breeze   |" ;
        }
    }
    if(n->east)
    {
        if(!n->east->hasPit)
        {
            n->east->breeze = true;
            n->east->s[5] = "|    Breeze   |" ;
        }
    }
    if(n->nWest)
    {
        if(!n->nWest->hasPit)
        {
            n->nWest->breeze = true;
            n->nWest->s[5] = "|    Breeze   |" ;
        }
    }
    if(n->nEast)
    {
        if(!n->nEast->hasPit)
        {
            n->nEast->breeze = true;
            n->nEast->s[5] = "|    Breeze   |" ;
        }
    }
    if(n->sEast)
    {
        if(!n->sEast->hasPit)
        {
            n->sEast->breeze = true;
            n->sEast->s[5] = "|    Breeze   |" ;
        }
    }
    if(n->sWest)
    {
        if(!n->sWest->hasPit)
        {
            n->sWest->breeze = true;
            n->sWest->s[5] = "|    Breeze   |" ;
        }
    }
}

void Map::setGold(ifstream &file)
{
    string strX="";
    string strY="";
    string goldline = "";
    // get string that has gold position
    // then remove spaces+
    getline(file, goldline);
    string gold = removeSpaces(goldline);
    
    strX = gold[0];
    strY = gold[1];      
    goldY = atoi(strY.c_str());
    goldX = atoi(strX.c_str());
        
    nodes[goldX][goldY].hasGold = true;
    nodes[goldY][goldX].s[3] =  "|    Glitter  |" ;
}

void Map::setWumpus(ifstream &file)
{
    string strX="";
	string strY="";
    string wumpusline = "";
    getline(file, wumpusline);
    string wumpus = removeSpaces(wumpusline);	        
    int wX =0, wY=0;
    strX= wumpus[0];
    strY= wumpus[1];		
    wY = atoi(strX.c_str());
    wX = atoi(strY.c_str());
    nodes[wX][wY].hasWumpus = true;
    nodes[wX][wY].s[1] =   "|    Wumpus   |" ;
}

void Map::printGrid()
{ 
    cout << "\e[8;70;180;t";
    string gridString = "";
    x = 5;
    cout << endl << endl;
    ostringstream oss;
    
   // cout << "\t" ;
    for(int i =0;i<n;i++)
        cout << "       " << i << "       ";
    cout << endl;
    
    for(int iii=0;iii<n;iii++)
    {  
        
        for(int bbb=0;bbb<8;bbb++)
        {
            
            for(int jjj=0;jjj<n;jjj++)
            {

                if(jjj==0 )
                {    
                    
                    if( bbb == 4)
                     {
                        oss << iii;
                        string temp;
                        temp = oss.str();
                        oss.str("") ;
                        gridString +=  " " + temp + "    ";
                        
                     }
                     else
                        gridString +=  "      ";
                }
              
                if(iii>0 && bbb == 0)
                    ;
                else
                    gridString += nodes[iii][jjj].s[bbb];

                if(jjj < n-1)
                {
                    string subStr = gridString.substr(0,gridString.length()-1);
                    gridString = subStr;
                }
               
            }
                gridString+= '\n';
        }
    }
    cout << gridString << endl;
    

   
}
/*
    Map ConnectNodes()
    Input:: none;
    Output:: none
    
    A nested for loop that links the nodes in every direction possible for its
    location in the grid. 
*/

void Map::connectNodes()
{
    for(int i=0;i<n;i++)
    {
        for(int j=0;j<n;j++)
        {
        
                    if(i+1 < n && j+1 < n )
					{
					//connec;ts south to north
					  nodes[i][j].south = &nodes[i+1][j];
					  nodes[i+1][j].north = &nodes[i][j];
					//connects south East to North Westb or Diagonally
					  //nodes[i][j].sEast = &nodes[i+1][j+1];
					  nodes[i+1][j+1].nWest = &nodes[i][j];
					//Connects east to west, or horizontally
					  nodes[i][j].east = &nodes[i][j+1];
					  nodes[i][j+1].west = &nodes[i][j];
				 
					}
					if(i > 0 && j+1 < n)
					{   // North East <-> South West
						nodes[i][j].nEast = &nodes[i-1][j+1];
						nodes[i-1][j+1].sWest = &nodes[i][j];
					}
					if(i > 0 && j > 0)
					{  // North West to South Easst
						nodes[i][j].nWest = &nodes[i-1][j-1];
						nodes[i-1][j-1].sEast = &nodes[i][j];
					}
                    // Specic connections, namely outer edges when
                    // i and j are the exact size of the grid
					if(j+1 == n && i+1 < n)
					{
					  nodes[i][j].south = &nodes[i+1][j];
					}
					if(j+1 == n && i > 0)
					{
						nodes[i][j].north = &nodes[i-1][j];
					}
					if(i+1 == n && j+1 < n)
					{
					    
					    nodes[i][j].east = &nodes[i][j+1];
					    nodes[i][j+1].west = &nodes[i][j];
					}
        
        }//End For J loop
    }//End FOr I Loop
}


void Map::setBreeze()
{
    
    for(int i = 0; i < n; i++)
    {
        for(int j = 0; j < n; j++)
        {
            //find the node with the PIT
            if(nodes[i][j].hasPit == true)
            {
                //Set all nodes around it to nearPit
                //Set the breeze string in the nodes
                //IF node has pit, do not set a Breeze
                if(nodes[i][j].north)
                {
                    if(!nodes[i-1][j].hasPit)
                    {
                        nodes[i-1][j].breeze = true;
                        nodes[i-1][j].s[5] = "|    Breeze   |" ;
                    }
                }
                if(nodes[i][j].south)
                {
                    if(!nodes[i+1][j].hasPit)
                    {
                        nodes[i+1][j].breeze = true;
                        nodes[i+1][j].s[5] = "|    Breeze   |" ;
                    }
                }
                if(nodes[i][j].east)
                {
                    if(!nodes[i][j+1].hasPit)
                    {
                        nodes[i][j+1].breeze = true;
                        nodes[i][j+1].s[5] = "|    Breeze   |" ;
                    }
                }
                if(nodes[i][j].west)
                {
                    if(!nodes[i][j-1].hasPit)
                    {
                        nodes[i][j-1].breeze = true;
                        nodes[i][j-1].s[5] = "|    Breeze   |" ;
                    }
                }
                if(nodes[i][j].nWest)
                {
                    if(!nodes[i-1][j-1].hasPit)
                    {
                        nodes[i-1][j-1].breeze = true;
                        nodes[i-1][j-1].s[5] = "|    Breeze   |" ;
                    }
                }
                if(nodes[i][j].sWest)
                {
                    if(!nodes[i+1][j-1].hasPit)
                    {
                        nodes[i+1][j-1].breeze = true;
                        nodes[i+1][j-1].s[5] = "|    Breeze   |" ;
                    }
                }
                if(nodes[i][j].nEast)
                {
                    if(!nodes[i-1][j+1].hasPit)
                    {
                        nodes[i-1][j+1].breeze = true;
                        nodes[i-1][j+1].s[5] = "|    Breeze   |" ;
                    }
                }
                if(nodes[i][j].sEast)
                {
                    if(!nodes[i+1][j+1].hasPit)
                    {
                        nodes[i+1][j+1].breeze = true;
                        nodes[i+1][j+1].s[5] = "|    Breeze   |" ;
                    }
                }
            }
        }	
    }
}

void Map::setStench()
{
    for(int i =0; i < n; i++)
    {
        for(int j = 0; j < n; j++)
        {
            if(nodes[i][j].hasWumpus == true)
            {
                //Set all nodes around it to Stench
                if(nodes[i][j].north)
                {
                    nodes[i-1][j].stench = true;
                    nodes[i-1][j].s[4] = "|    Stench   |" ;
                    
                }
                if(nodes[i][j].south)
                {
                    nodes[i+1][j].stench = true;
                    nodes[i+1][j].s[4] = "|    Stench   |" ;
                }
                if(nodes[i][j].east)
                {
                    nodes[i][j+1].stench = true;
                    nodes[i][j+1].s[4] = "|    Stench   |" ;
                }
                if(nodes[i][j].west)
                {
                    nodes[i][j-1].stench = true;
                    nodes[i][j-1].s[4] = "|    Stench   |" ;
                }
                if(nodes[i][j].nWest)
                {
                    nodes[i-1][j-1].stench = true;
                    nodes[i-1][j-1].s[4] = "|    Stench   |" ;
                }
                if(nodes[i][j].sWest)
                {
                    nodes[i+1][j-1].stench = true;
                    nodes[i+1][j-1].s[4] = "|    Stench   |" ;
                }
                if(nodes[i][j].nEast)
                {
                    nodes[i-1][j+1].stench = true;
                    nodes[i-1][j+1].s[4] = "|    Stench   |" ;
                }
                if(nodes[i][j].sEast)
                {
                    nodes[i+1][j+1].stench = true;
                    nodes[i+1][j+1].s[4] = "|    Stench   |" ;
                }
            }
        }
    }
}




#endif
