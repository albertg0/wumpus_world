#include <iostream>
#include <string>
#include <ostream>
#include <fstream>
#include <cstring>
#include <cstdlib>
#include <algorithm>
#include <vector>
#include "node.h"
#include "Map.h"
#include "Character.h"

//Dagoberto Rodriguez
//Alberto Garcia
//Assignment 1		Feb 1
/*
    Wumpus World
    Most of the classes have been split into their own Header and Implementation files.
    The node.h file describes the individual node and its directions
    The Map.h file connect all the nodes and is based off an adjency matrix
    The Character.h file defines the agent and its attributes
    The Wumpus World class was left in main as we used it to work all our new code since
    we created header files for all the working code.
    
    The program begins in main (line 641) by calling the Wumpus World object to run.
    The map contrusctor takes the config file and creates the initial map. 
    The wumpus world then works on the map to create the game.
    
*/
using namespace std;

//Wumpus World Class
class WumpusWorld : Map {
    
    Map m;
    bool running, wumpusAlive;
    string user_input;
    int score;
   
    
public:
     
    //Constructor  
    WumpusWorld()
    {
        score = 0;
        running = wumpusAlive = true;
    	m = Map();
      
    }

    //Game starts here,  list of commands the game is programmed to take 
    //then calls the approppriate function that handles the rest.
    //Most functions aall void
	void run()
	{
	  int count = 0;
        	
		while(running)
		{
            		m.x = 5;
            		m.y = 0;
			cout << "\033[2J";
         
         		/****************************************************
         		*	prints the map each time through the loop
         		*	and also prints the rest of the required
         		*	fields
         		*****************************************************/
        	 	m.printGrid(); 			
            		percepts();
            		current_location();
            		cout<<"Current Score: "<< score << endl;
            		
            		//function that checks what item the agent is currently holding
            		check_Agent();
            		cin >> user_input;
            
            
            		    //Q will exit the game
			    if(user_input.compare("Q") == 0 || user_input.compare("q") == 0)
			    {
					running = false;
			    }
			    //rotates player right, sets score -1
			    else if(user_input.compare("R") == 0 || user_input.compare("r") == 0)
			    {
		        	score -= 1;
		        	m.setOrientation("R");
				rotatePlayer("R");
			    }
			    //rotates player left, sets score -1
			    else if(user_input.compare("L") == 0  || user_input.compare("l") == 0)
			    {
				score -= 1;
				m.setOrientation("L");
				rotatePlayer("L");
			    }
			    //Drop the item currently holding
			    else if(user_input.compare("D") == 0 || user_input.compare("d") == 0)
			    {
				score -= 1;
				agent.drop();
				
			    }
			    //Grab an item from the node
			    else if(user_input.compare("G") == 0 || user_input.compare("g") == 0)
			    {
				score -= 1;
				agent.grab();
			    }
			    //Moves the player forward
			    else if(user_input.compare("F")== 0  || user_input.compare("f") == 0)
			    {
				score -= 1;
				moveForward();
			    }
			    //Will shoot arrow. Only works once.
			    else if(user_input.compare("S")== 0  || user_input.compare("s") == 0)
			    {
			    	if(agent.hasBow)
			    	{
				    	if(count < 1)
				    	{
				    		score -=10;
				    		shootArrow();
				    		count++;
				    	}
				    	else
				    		cout<<"You dont have anymore arrows" << endl;
				 }
				 else
				 	cout<<"You dont have the bow" << endl;
			    }
			    //Exits cave, if have gold then +1000, else it just ends game
			    else if(user_input.compare("C")== 0  || user_input.compare("c") == 0)
			    {
				running = exitCave();
				if(agent.hasGold)
				{
					score += 1000;
				}
				if(!running)
				{
				    break;
				}
			    }
			    else
			    {
				cout << "Please choose etc\n";
			    }
			    
			    
			    //check_game function returns false if player falls on pit, or wumpus node.
			    running = check_game();
			    if(running)
			    {
				
			    }
			    else
				score -= 1000;
				
			    
			    
			  
		}
            m.x = 5;
            m.y = 0;
	    cout << "\033[2J";
         
	    m.printGrid();
            percepts();
            current_location();
            cout<<"Current Score: "<< score << endl;
	}

//Prints the current player location to the map
void current_location()
{
	int x, y;
	x = agent.getPosX();
	y = agent.getPosY();
	
	cout<<"Current Location: (" << x << "," << y << ")"<<endl;
			

}
//Percepts defined in the file by moving around
void percepts()
{
	cout<<"Percepts: <St, Br, G, Bu, Sc> = ";
	int x, y;
	x = agent.getPosX();
	y = agent.getPosY();
	
	if(m.nodes[y][x].stench)
	{
		cout<<"<1, ";
	}
	else
		cout<<"<0, ";
	if(m.nodes[y][x].breeze)
	{
		cout<<"1, ";
	}
	else
		cout<<"0, ";
	if(m.nodes[y][x].hasGold)
	{
		cout<<"1, ";
	}
	else
		cout<<"0, ";
        if(m.nodes[y][x].hasBumped)
	{
		cout<<"1, ";
	}
	else 
		cout<<"0, ";
        if(!wumpusAlive)
        	cout<<"1>"<<endl;
	else
		cout<<"0>" << endl;
	
	
        m.nodes[x][y].hasBumped = false;
	
	
	

}
//Returns false if you die
bool check_game()
{
	int x, y;
	//agents current position
	x = agent.getPosX();
	y = agent.getPosY();
	
	if(m.nodes[y][x].hasPit)
	{
		return false;				
	}
	else if(m.nodes[y][x].hasWumpus)
	{
		return false;
	}
	else
		return true;

}
//Movement function, player is printed out into node box
  void moveForward()
    {
        
       int i, j;
	j = agent.getPosX();
	i = agent.getPosY();
				if(m.getOrientation()  == 0)
				{
				  	//move player north if available
					if(m.nodes[i][j].north)
					{
					 	removeOldPosition(j, i);
						m.nodes[i][j].hasPlayer = false;
						m.setPlayerString(j,i-1,m.orientation);
						
						//Stores in record agents new position    
						agent.setPosX(j);
						agent.setPosY(i-1);
						agent.setCurrentNode(&nodes[j][i-1]);
					 }
					 else
					 {
						m.nodes[i][j].hasBumped = true;
					     	cout<< "YOU CANT GO THERE" << endl;
					 }
				  }
				else if(m.getOrientation() == 1)
				{
					 //move player north east if available
					  if(m.nodes[i][j].nEast)
					  {
					   	removeOldPosition(j, i);
						m.nodes[i][j].hasPlayer = false;
						m.setPlayerString(j+1,i-1,m.orientation);
						                
						agent.setPosX(j+1);
						agent.setPosY(i-1);
						agent.setCurrentNode(&nodes[j+1][i-1]);
					   }
					   else
					   {
						m.nodes[i][j].hasBumped = true;
						cout<< "YOU CANT GO THERE" << endl;
					   }
						       			
				}
				else if(m.getOrientation()  == 2)
				{
					  //move player East if available
					  if(m.nodes[i][j].east)
					  {
						removeOldPosition(j, i);
						m.nodes[i][j].hasPlayer = false;
						m.setPlayerString(j+1,i,m.orientation);
						                   
					       	agent.setPosX(j+1);
						agent.setPosY(i);
						agent.setCurrentNode(&nodes[j+1][i]);
					   }
				       	  else
					  {
						m.nodes[i][j].hasBumped = true;
						cout<< "YOU CANT GO THERE" << endl;
					  }
				 }
                                else if(m.getOrientation()  == 3)
                                {
                                	//move player south East if available
                                	if(m.nodes[i][j].sEast)
                                	{
                                		removeOldPosition(j, i);
                                		m.nodes[i][j].hasPlayer = false;
                               			m.setPlayerString(j+1,i+1,m.orientation);
                                           
                                        agent.setPosX(j+1);
                                        agent.setPosY(i+1);
                                        agent.setCurrentNode(&nodes[j+1][i+1]);
                               		}
                               		else
                               		{
                                      		m.nodes[i][j].hasBumped = true;
		                       		cout<< "YOU CANT GO THERE" << endl;
		                       	}
                               		
                                }
                                else if(m.getOrientation()  == 4)
                                {
                                	//move player south if available
                                	if(m.nodes[i][j].south)
                                	{
                                		removeOldPosition(j, i);
                                		m.nodes[i][j].hasPlayer = false;
                               			m.setPlayerString(j,i+1,m.orientation);
                                        
                                        agent.setPosX(j);
                                        agent.setPosY(i+1);
                                        agent.setCurrentNode(&nodes[j][i+1]);
                               		}
                               		else
                               		{
                                       		m.nodes[i][j].hasBumped = true;
		                       		cout<< "YOU CANT GO THERE" << endl;
		                       	}
                                }
                                else if(m.getOrientation()  == 5)
                                {
                                	//move player south West if available
                                	if(m.nodes[i][j].sWest)
                                	{
                                		removeOldPosition(j, i);
                                		m.nodes[i][j].hasPlayer = false;
                               			m.setPlayerString(j-1,i+1,m.orientation);
                                        agent.setPosX(j-1);
                                        agent.setPosY(i+1);
                                        agent.setCurrentNode(&nodes[j-1][i+1]);
                               		}
                               		else
                               		{
                                       		m.nodes[i][j].hasBumped = true;
		                       		cout<< "YOU CANT GO THERE" << endl;
		                       	}
                                }
                                else if(m.getOrientation() == 6)
                                {
                                	//move player West if available
                                	if(m.nodes[i][j].west)
                                	{
                                		removeOldPosition(j, i);
                                		m.nodes[i][j].hasPlayer = false;
                               			m.setPlayerString(j-1,i,m.orientation);
                                        agent.setPosX(j-1);
                                        agent.setPosY(i);
                                        agent.setCurrentNode(&nodes[j-1][i]);
                               		}
                               		else
                               		{
                                       		m.nodes[i][j].hasBumped = true;
		                       		cout<< "YOU CANT GO THERE" << endl;
		                       	}
                                }
                                else if(m.getOrientation() == 7)
                                {
                                	//move player North West if available
                                	if(m.nodes[i][j].nWest)
                                	{        
                                		removeOldPosition(j, i);
                                		m.nodes[i][j].hasPlayer = false;                       
                               			m.setPlayerString(j-1,i-1,m.orientation);
                                        agent.setPosX(j-1);
                                        agent.setPosY(i-1);
                                        agent.setCurrentNode(&nodes[j-1][i-1]);
                               		}
                               		else
                               		{
                                       		m.nodes[i][j].hasBumped = true;
		                       		cout<< "YOU CANT GO THERE" << endl;
		                       	}
                                }
                                
                                return;
                
        
    }
    //Removes player from previous node
     void removeOldPosition(int posX, int posY)
    {
        m.nodes[posY][posX].s[6] = "|             |" ;
    }
		
    

    // Rotates the player    
    void rotatePlayer(string direction)
    {
       int x, y;
	x = agent.getPosX();
	y = agent.getPosY();

	m.setPlayerString(x,y, m.getOrientation());			

			
	   
     }
     //Checks what item the agent is currently holding
     void check_Agent()
     {
     	if(agent.hasBow)
     	{
     		cout<<"Agent has bow" << endl;
     	}
     	else if(agent.hasGold)
     	{
     		cout<<"Agent has gold" << endl;
     	}
     	else
     		cout<<"Agent has nothing" << endl;
     }
     
     
     //function that shoots Agents' arrow
     void shootArrow()
     {
     	int i,j, x, y;
     	x = agent.getPosX();
     	y = agent.getPosY();
     	
     	i = j = 0;
	
	
    	//Check orientation
    	if(m.getOrientation() == 0)
    	{
    		//Check every node in that direction
    		while(m.nodes[y-i][x].north)
    		{
    			//If wumpus not in any of them
    			//hit  wall, else
    			//kill wumpus and scream everywheres
    			if(m.nodes[y-i][x].north->hasWumpus)
    			{
    				wumpusAlive = false;
    				m.nodes[y-i][x].north->hasWumpus = false;
    				exit;
    			}
    				
    			i++;
    		}
    	}
    	//Check orientation
    	if(m.getOrientation() == 1)
    	{
    		//Check every node in that direction
    		while(m.nodes[y-i][x+j].nEast)
    		{
    			//If wumpus not in any of them
    			//hit  wall, else
    			//kill wumpus and scream everywheres
    			if(m.nodes[y-i][x+j].nEast->hasWumpus)
    			{
    				wumpusAlive = false;
    				m.nodes[y-i][x+j].nEast->hasWumpus = false;
    				exit;
    			}
    			i++;
    			j++;
    		}
    	}
    	//Check orientation
    	if(m.getOrientation() == 2)
    	{
    		//Check every node in that direction
    		while(m.nodes[y][x+j].east)
    		{
    			//If wumpus not in any of them
    			//hit  wall, else
    			//kill wumpus and scream everywheres
    			if(m.nodes[y][x+j].east->hasWumpus)
    			{
    				wumpusAlive = false;
    				m.nodes[y][x+j].east->hasWumpus = false;
    				exit;
    			}
    				
    			j++;
    		}
    	}
    	//Check orientation
    	if(m.getOrientation() == 3)
    	{
    		//Check every node in that direction
    		while(m.nodes[y+i][x+j].sEast)
    		{
    			//If wumpus not in any of them
    			//hit  wall, else
    			//kill wumpus and scream everywheres
    			if(m.nodes[y+i][x+j].sEast->hasWumpus)
    			{
    				wumpusAlive = false;
    				m.nodes[y+i][x+j].sEast->hasWumpus = false;
    				exit;
    			}
    			j++;
    			i++;
    		}
    	}
    	//Check orientation
    	if(m.getOrientation() == 4)
    	{
    		//Check every node in that direction
    		while(m.nodes[y+i][x].south)
    		{
    			//If wumpus not in any of them
    			//hit  wall, else
    			//kill wumpus and scream everywheres
    			if(m.nodes[y+i][x].south->hasWumpus)
    			{
    				wumpusAlive = false;
    				m.nodes[y+i][x].south->hasWumpus = false;
    				exit;
    			}
    				
    			i++;
    		}
    	}
    	//Check orientation
    	if(m.getOrientation() == 5)
    	{
    		//Check every node in that direction
    		while(m.nodes[y+i][x-j].sWest)
    		{
    			//If wumpus not in any of them
    			//hit  wall, else
    			//kill wumpus and scream everywheres
    			if(m.nodes[y+i][x-j].sWest->hasWumpus)
    			{
    				wumpusAlive = false;
    				m.nodes[y+i][x-j].sWest->hasWumpus = false;
    				exit;
    			}
    			j++;
    			i++;
    		}
    	}
    	//Check orientation
    	if(m.getOrientation() == 6)
    	{
    		//Check every node in that direction
    		while(m.nodes[y][x-i].west)	
    		{
    			//If wumpus not in any of them
    			//hit  wall, else
    			//kill wumpus and scream everywheres
    			if(m.nodes[y][x-i].west->hasWumpus)
    			{
    				wumpusAlive = false;
    				m.nodes[y][x-i].west->hasWumpus = false;
    				exit;
    			}
    				
    			i++;
    		}
    	}
    	//Check orientation
    	if(m.getOrientation() == 7)
    	{
    		//Check every node in that direction
    		while(m.nodes[y-i][x-j].nWest)
    		{
    			//If wumpus not in any of them
    			//hit  wall, else
    			//kill wumpus and scream everywheres
    			if(m.nodes[y-i][x-j].nWest->hasWumpus)
    			{
    				wumpusAlive = false;
    				m.nodes[y-i][x-j].nWest->hasWumpus = false;
    				exit;
    			}
    			j++;
    			i++;
    		}
    	}
    
    
    //
     }
    
     //returns false if agent is at starting location
     bool exitCave()
     {
     	
     		if(agent.getPosX() == m.startLocX && agent.getPosY() == m.startLocY)
     		{
     			cout<<"You have exited the cave" << endl;
     			return false;
     		}
     	
     		
     		
	     	else
	     	 return true;   
     
     }


};

   
int main()
{
   

	WumpusWorld w;
	
    	w.run();
	
	
	
	return 0;

}



